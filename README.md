# ERC20

EIP20 Specification - https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md

# Tasks

```shell
npx hardhat compile
npx hardhat test
npx hardhat help
npx hardhat coverage
npx hardhat run scripts/deploy.ts
```

```shell
npx hardhat tranfer --to "address" --value "amount"
npx hardhat approve --to "address" --value "amount"
npx hardhat transferFrom --from "address" --to "address" --value "amount"
```

# Example

https://rinkeby.etherscan.io/address/0xdf0C43c8E865c98080130b083fdE9d397a4618BA
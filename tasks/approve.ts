import { task } from "hardhat/config";
import { parseToken } from "./shared";

interface TaskArgs {
  to: string;
  value: string;
}

task("approve", "Approve tokens to address")
  .addParam("to", "Spender")
  .addParam("value", "Amount")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const Token = await hre.ethers.getContractAt(
      "Token",
      String(process.env.CONTRACT_ADDRESS)
    );
    const tx = await Token.approve(
      taskArgs.to,
      parseToken(hre.ethers, taskArgs.value)
    );
    await tx.wait();

    console.log(`Tx hash: ${tx.hash}`);
  });

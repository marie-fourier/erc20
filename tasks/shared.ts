require("dotenv").config();

const _decimals = 8;

export const contractAddress = String(process.env.CONTRACT_ADDRESS);

export const parseToken = (ethers: any, value: string) => {
  return ethers.BigNumber.from(value).mul(10 ** _decimals);
};
